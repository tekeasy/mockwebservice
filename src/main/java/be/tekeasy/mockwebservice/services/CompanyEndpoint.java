package be.tekeasy.mockwebservice.services;

import be.tekeasy.mockwebservice.repositories.CompanyRepository;
import https.tekeasy_be.companyservice.GetCompanyDetailsRequest;
import https.tekeasy_be.companyservice.GetCompanyDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CompanyEndpoint {
    private static final String NAMESPACE_URI = "https://tekeasy.be/CompanyService";

    private CompanyRepository companyRepository;

    @Autowired
    public CompanyEndpoint(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCompanyDetailsRequest")
    @ResponsePayload
    public GetCompanyDetailsResponse getCompany(@RequestPayload GetCompanyDetailsRequest request) {
        GetCompanyDetailsResponse response = new GetCompanyDetailsResponse();
        response.setCompany(companyRepository.findCompany(request.getName()));
        return response;
    }

}
