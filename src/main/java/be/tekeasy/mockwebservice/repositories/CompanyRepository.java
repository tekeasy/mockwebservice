package be.tekeasy.mockwebservice.repositories;

import https.tekeasy_be.companyservice.Address;
import https.tekeasy_be.companyservice.Company;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class CompanyRepository {
    private static final Map<String, Company> companies = new HashMap<>();

    @PostConstruct
    public void initData() {

        Address address = new Address();
        address.setCity("Lasne");
        address.setCountry("Belgium");
        address.setStreet("chaussée");
        address.setZip(1380);

        Company ucb = new Company();
        ucb.setName("United Kingdom");
        ucb.setAddress(address);
        ucb.setEmail("email ucb");
        ucb.setId(1);
        companies.put(ucb.getName(), ucb);

        Company tekeasy = new Company();
        tekeasy.setName("tekeasy");
        tekeasy.setAddress(address);
        tekeasy.setEmail("email tekeasy");
        tekeasy.setId(2);

        companies.put(tekeasy.getName(), tekeasy);
    }

    public Company findCompany(String name) {
        Assert.notNull(name, "The company's name must not be null");
        return companies.get(name);
    }

}
