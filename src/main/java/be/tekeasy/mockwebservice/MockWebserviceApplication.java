package be.tekeasy.mockwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockWebserviceApplication.class, args);
	}

}
